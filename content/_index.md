+++
title = "Home"
+++

# Home

Welcome to the home page of this website!

# Lo spettacolo

L’associazione Rec Accademia Musicale in collaborazione con l’associazione Battiti di Danza e l’orchestra Zephyrus ha ideato uno spettacolo destinato al pubblico tratto dalle più celebri arie d’opera e di balletto. Dall’unione di grandi professionisti dei maggiori teatri italiani con gli studenti dei percorsi accademici e professionali nasce quindi una grande occasione culturale e artistica che ha lo scopo di dare spazio ai giovani talenti avvicinando il pubblico al balletto, seguendo la convinzione che l’arte debba essere un patrimonio di universale fruizione.

# Ospiti

## Martina Arduino
Prima ballerina dalla grazia delicata, piemontese ma nata e cresciuta all'Accademia scaligera e oggi prima ballerina del Teatro la Scala di Milano

## Marco Agostino
Dopo essersi diplomato alla Scuola di Ballo della Scala entra nel Corpo di Ballo danzando in molti ruoli principali. Al concorso del 2022, sotto la direzione di Manuel Legris, viene nominato Primo Ballerino
# Corpo di Ballo
L’associazione Rec Accademia Musicale in collaborazione con l’associazione Battiti di Danza e l’orchestra Zephyrus ha ideato uno spettacolo destinato al pubblico tratto dalle più celebri arie d’opera e di balletto. Dall’unione di grandi professionisti dei maggiori teatri italiani con gli studenti deipatrimonio di universale fruizione.

## Soliste
Sofia Tracchi
Francesca Chiesa
Lesleey Pilozo
Ludovica Pigliafreddo
Giorgia Fiorentino

## Ensamble
Giorgia Longobardi 
Martina Pettinari
Sonia Ramundo 
Malica Goldin Bombonati
Sofia Mobelli 
Ilaria fiorentino
Sofia Carsenzuola
Ylenia La Guidara 
Arianna Tolusso
Martina Pettinari
Sofia Mombelli
Melania Casser 
Emma Fagnani
Sofia Carsenzuola 
Sofia Tracchi 
Giorgia Longobardi 
Ilaria Fiorentino
Arianna Tolusso
Leesley Pilozo

## Coreografie e Arrangiamenti Coreografici
Rossella Buso, Elena De Laurentiis, Ilaria Miotto, Denise Conti

# L'orchestra
L’associazione Rec Accademia Musicale in collaborazione con l’associazione Battiti di Danza e l’orchestra Zephyrus ha ideato uno spettacolo destinato al pubblico tratto dalle più celebri arie d’opera e di balletto. Dall’unione di grandi professionisti dei maggiori teatri italiani con gli studenti dei percorsi accademici e professionali nasce quindi una grande occasione culturale e